FROM ubuntu:18.04

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
    software-properties-common


RUN add-apt-repository universe
RUN apt-get update && apt-get install -y \
     curl \
     git \
     python3-dev \
     python3.6 \
     python3-pip \
     libffi-dev \
     git \
     netcat


RUN /usr/bin/pip3 install psycopg2-binary


RUN mkdir /app
COPY . /app
WORKDIR /app

RUN /usr/bin/pip3 install -r prod_requirements.txt

ENTRYPOINT ["sh", "entrypoint.sh"]

