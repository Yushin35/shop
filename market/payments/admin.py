from django.contrib import admin

from market.products.models import Order, OrderItem, Checkout


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'u_id',
        'total',
        'paid',
        'approved',
        'progress',
        'payment_type',
        'json_response',
        'payment_id',
        'checkout',
    )


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'order',
        'product',
        'price',
        'description',
        'subcategory',
        'quantity',
        'total',
    )


@admin.register(Checkout)
class CheckoutItemAdmin(admin.ModelAdmin):
    pass
