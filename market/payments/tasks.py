from django.core.mail import send_mail

from config.settings import ADMIN_EMAIL
from market.common.email_utils import (
    purchase_mailing_body_generator,
    complete_purchase_body_generator,
)
from market.users.models import User

from celery import shared_task


@shared_task(
    ignore_result=False
)
def purchase_mailing(email_address, order_pk):
    subject = 'Purchase in shop'
    body = purchase_mailing_body_generator(order_pk)
    send_mail(
        subject,
        body,
        ADMIN_EMAIL,
        [email_address],
        fail_silently=False,
    )


@shared_task(
    ignore_result=False
)
def complete_purchase():
    users = User.objects.filter(
        cart_items__isnull=False
    )

    subject = 'Complete purchase in shop'

    for user in users:
        body = complete_purchase_body_generator(user)
        send_mail(
            subject,
            body,
            ADMIN_EMAIL,
            [user.email]
        )

