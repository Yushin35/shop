import uuid

from django.views.generic import TemplateView, ListView, View
from django.views.generic.detail import (
    SingleObjectMixin,
    SingleObjectTemplateResponseMixin,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.shortcuts import render, HttpResponse
from django.http import Http404

from market.products.models import CartItem, OrderItem
from market.products.cart import Cart
from market.payments.tasks import purchase_mailing


import stripe
import paypalrestsdk
from liqpay import LiqPay


from market.products.models import (
    Order,
)

from market.payments.forms import CheckoutForm
from market.common.utils import (
    get_carts_total,
    create_order_items,
    create_paypal_payment_data,
)

from config import settings
from config.settings import paypal_api


PAYMENT_VIEWS = {
    Order.PAY_STRIPE: 'payments:pay_stripe',
    Order.PAY_PAYPAL: 'payments:pay_paypal',
    Order.PAY_LIQPAY: 'payments:pay_liqpay'
}


class CheckoutView(TemplateView):
    template_name = 'payments/checkout.html'

    def get(self, request, *args, **kwargs):
        return render(
            request,
            self.template_name,
            {
                'checkout_form': CheckoutForm()
            }
        )

    def post(self, request, *args, **kwargs):
        checkout_form = CheckoutForm(request.POST)
        if not checkout_form.is_valid():
            return render(
                request,
                self.template_name,
                {
                    'checkout_form': checkout_form
                }
            )
        cleaned_checkout_data = checkout_form.cleaned_data
        checkout = checkout_form.save()

        payment_type = cleaned_checkout_data.get('payment_type')
        cart = Cart(self.request)

        order = Order.objects.create(
            total=cart.get_total_price(),
            payment_type=payment_type,
            u_id=str(uuid.uuid4()),
            checkout=checkout,
        )
        order_items = [
            OrderItem.objects.create(
                order=order,
                product=product,
                name=product.name,
                code=product.code,
                price=product.price,
                description=product.description,
                subcategory=product.subcategory.name,
                quantity=product.quantity,
                total=product.total_price,
            )
            for product in cart.get_products()
        ]

        try:
            url_name = PAYMENT_VIEWS[payment_type]
        except KeyError:
            raise

        return redirect(
            reverse(
                url_name,
                kwargs={
                    'pk': order.pk
                }
            )
        )


class OrderListView(ListView):
    template_name = 'payments/order_list.html'
    model = Order
    context_object_name = 'order_list'

    def get_queryset(self):
        return super().get_queryset().filter(
            user=self.request.user,
            paid=False,
        )


class PaymentView(SingleObjectTemplateResponseMixin, SingleObjectMixin, View):
    model = Order
    context_object_name = 'order'

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['order_items'] = self.object.items.all().select_related('product')
        return context


class PayStripeView(PaymentView):
    template_name = 'payments/pay_stripe.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stripe_public_key'] = settings.STRIPE_PUBLIC_KEY
        context['total_cent'] = self.object.total * 100

        return context

    def post(self, request, *args, **kwargs):
        token = request.POST.get('stripeToken')
        order = self.get_object()
        charge = stripe.Charge.create(
            amount=int(order.total),
            currency='usd',
            description='Example charge',
            source=token,
        )
        charge.save()

        order.paid = True
        order.payment_type = Order.PAY_STRIPE
        order.payment_id = charge['id']

        order.save(update_fields=[
            'payment_type',
            'payment_id',
            'paid',
        ])
        purchase_mailing.delay(self.request.user.email, order.pk)
        return HttpResponseRedirect(reverse('home'))


class PayPalView(PaymentView):
    template_name = 'payments/pay_paypal.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['client_id'] = settings.PAYPAL_CLIENT_ID
        return context

    def post(self, request, *args, **kwargs):
        order = self.get_object()
        order_items = order.items.all().select_related('product')
        payment_data = create_paypal_payment_data(order_items, order)
        payment = paypalrestsdk.Payment(payment_data, api=paypal_api)
        if payment.create():
            order.payment_type = Order.PAY_PAYPAL
            order.payment_id = payment.id
            order.save(update_fields=['payment_type', 'payment_id'])
            
            return JsonResponse({
                'paymentID': payment.id
            })
        
        return JsonResponse({
            'paymentID': None
        })


class PayPalExecuteView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, *args, **kwargs):
        order = Order.objects.get(
            payment_type=Order.PAY_PAYPAL,
            payment_id=self.request.POST.get('paymentID'),
        )
        payment = paypalrestsdk.Payment.find(self.request.POST.get('paymentID'))
        if payment.execute({'payer_id': self.request.POST.get('payerID')}):
            order.paid = True
            order.save(update_fields=['paid'])
            purchase_mailing.delay(self.request.user.email, order.pk)
            return JsonResponse({
                'payment_status': 'ok'
            })
        return JsonResponse({
            'payment_status': 'fail'
        })


class PayView(PaymentView):
    template_name = 'payments/pay_liqpay.html'

    def get(self, request, *args, **kwargs):
        liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
        order = self.get_object()
        if order.paid:
            return HttpResponse("HTTP 403 status code. Restricted Area", status=403)

        params = {
            'action': 'pay',
            'amount': str(order.total),
            'currency': 'UAH',
            'description': 'Покупка электроники',
            'order_id': order.u_id,
            'version': '3',
            'sandbox': 0, # sandbox mode, set to 1 to enable it
            'server_url': settings.LIQPAY_CALLBACK_URL, # url to callback view
        }
        signature = liqpay.cnb_signature(params)
        data = liqpay.cnb_data(params)

        return render(
            request,
            self.template_name,
            {
                'signature': signature,
                'data': data,
                'order': order,
            }
        )


@method_decorator(csrf_exempt, name='dispatch')
class PayCallbackView(View):

    def post(self, request, *args, **kwargs):
        liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')

        sign = liqpay.str_to_sign(settings.LIQPAY_PRIVATE_KEY + data + settings.LIQPAY_PRIVATE_KEY)
        if sign == signature:
            response = liqpay.decode_data_from_str(data)
            order = Order.objects.get(u_id=response['order_id'])
            order.json_response = response
            status = response['status']
            if status == 'success' or \
                    status == 'wait_accept' or \
                    status == 'wait_compensation':
                order.paid = True
                order.payment_id = response['payment_id']
                order.save()
                return HttpResponse('ok')

            order.save()
            return HttpResponse('payment error: see detail', code=405)

        return HttpResponse('signature error', code=405)

