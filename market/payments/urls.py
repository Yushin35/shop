# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from market.payments import views


urlpatterns = [
    url(r'^checkout/$', views.CheckoutView.as_view(), name='checkout'),
    url(r'^orders/$', views.OrderListView.as_view(), name='orders'),
    url(r'^paystripe/(?P<pk>[0-9]+)/$', views.PayStripeView.as_view(), name='pay_stripe'),
    url(r'^paypal/(?P<pk>[0-9]+)/$', views.PayPalView.as_view(), name='pay_paypal'),
    url(r'^paypal/execute/$', views.PayPalExecuteView.as_view(), name='paypal_execute'),
    url(r'^pay/(?P<pk>[0-9]+)/$$', views.PayView.as_view(), name='pay_liqpay'),
    url(r'^pay-callback/$', views.PayCallbackView.as_view(), name='pay_liqpay_callback'),
]
