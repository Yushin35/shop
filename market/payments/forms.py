from django import forms
from market.products.models import Checkout


class OrderForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    name = forms.CharField(max_length=50, required=True)
    surname = forms.CharField(max_length=50, required=True)
    phone = forms.CharField(max_length=50, required=True)
    email = forms.EmailField(required=False)
    address = forms.CharField(max_length=255, required=True)


class CheckoutForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['fio'].required = False
        self.fields['email'].required = False
        self.fields['comment'].required = True

    class Meta:
        model = Checkout
        fields = (
            'phone',
            'fio',
            'email',
            'comment',
            'payment_type'
        )
