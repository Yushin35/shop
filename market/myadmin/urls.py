from django.conf.urls import include, url
from django.views.generic import TemplateView


from rest_framework_nested import routers
from rest_framework_swagger.views import get_swagger_view


from market.myadmin.views import (
    OrderViewSet,
    OrderItemViewSet,
)


schema_view = get_swagger_view(title='Pastebin API')  # pylint: disable=invalid-name


simple_router = routers.SimpleRouter()

simple_router.register(r'orders', OrderViewSet, basename='orders')
simple_router.register(r'order_items', OrderItemViewSet, basename='order_items')


urlpatterns = [
    url(r'^', include(simple_router.urls)),
    url(r'^$', schema_view),
    url(r'^index/$', TemplateView.as_view(template_name='myadmin/index.html'), name='myadmin'),
]

