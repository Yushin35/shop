from rest_framework import permissions


class IsAdminOrFYou(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user.is_staff

    def has_permission(self, request, view):
        return request.user.is_staff


class IsAuthenticatedOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user.is_staff or obj.owner == request.user

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user.is_authenticated
