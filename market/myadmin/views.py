from rest_framework.authentication import BasicAuthentication
from rest_framework import viewsets, views

from market.products.models import (
    Order,
    OrderItem,
)

from market.myadmin.pagination import StandardResultsSetPagination
from market.myadmin.permissions import IsAdminOrFYou
from market.myadmin.filters import (
    StatusFilterBackend,
    PaidStatusFilterBackend,
    OrderItemOrderPkFilterBackend,
)
from market.myadmin.serializers import (
    OrderSerializer,
    OrderItemSerializer,
)


class OrderViewSet(viewsets.ModelViewSet):
    authentication_classes = (BasicAuthentication, )
    permission_classes = (IsAdminOrFYou, )
    serializer_class = OrderSerializer
    queryset = Order.objects.all().order_by('-id')
    filter_backends = (
        StatusFilterBackend,
        PaidStatusFilterBackend
    )
    pagination_class = StandardResultsSetPagination

    def get_queryset(self, *arg, **kwargs):
        # from time import sleep
        # sleep(1)
        return super().get_queryset(*arg, **kwargs)


class OrderItemViewSet(viewsets.ModelViewSet):
    authentication_classes = (BasicAuthentication, )
    permission_classes = (IsAdminOrFYou, )
    serializer_class = OrderItemSerializer
    queryset = OrderItem.objects.all()
    filter_backends = (
        OrderItemOrderPkFilterBackend,
    )
    pagination_class = StandardResultsSetPagination

