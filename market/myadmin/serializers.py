from django.conf import settings

from rest_framework.serializers import ModelSerializer
from rest_framework import serializers


from market.products.models import (
    Order,
    OrderItem,
    Checkout,
    Product,
    Category,
    SubCategory,
)


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'pk',
            'name',
            'slug',
        )


class SubCategorySerializer(ModelSerializer):
    category = CategorySerializer(read_only=True)

    class Meta:
        model = SubCategory
        fields = (
            'pk',
            'slug',
            'category',
        )


class ProductSerializer(ModelSerializer):
    subcategory = SubCategorySerializer(read_only=True)

    class Meta:
        model = Product
        fields = (
            'pk',
            'name',
            'slug',
            'description',
            'subcategory',
        )


class CheckoutSerializer(ModelSerializer):
    product = ProductSerializer(read_only=True)

    class Meta:
        model = Checkout
        fields = (
            'pk',
            'phone',
            'payment_type',
            'fio',
            'email',
            'comment',
            'product',
        )


class OrderSerializer(ModelSerializer):
    checkout = CheckoutSerializer(read_only=True, required=False)

    class Meta:
        model = Order
        fields = (
            'pk',
            'total',
            'paid',
            'payment_type',
            'payment_id',
            'u_id',
            'json_response',
            'checkout',
            'approved',
            'progress',
            'datetime',
        )
        read_only_fields = [
            'total',
            'u_id',
            'payment_type'
        ]


class OrderItemSerializer(ModelSerializer):
    class Meta:
        model = OrderItem
        fields = (
            'pk',
            'code',
            'name',
            'price',
            'subcategory',
            'quantity',
            'total',
        )
