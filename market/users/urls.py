from django.conf.urls import url
from django.contrib.auth import views as auth_views

from market.users.views import RoomView, RegisterView

urlpatterns = [
    url(r'^room/$', RoomView.as_view(), name='room'),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
]
