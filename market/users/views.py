from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from django.views import generic
from django.urls import reverse_lazy
from config import settings

from market.products.forms import UserForm


class RoomView(LoginRequiredMixin, TemplateView):
    active_tab = 'cart'
    template_name = 'users/room.html'


class RegisterView(generic.CreateView):
    model = settings.AUTH_USER_MODEL
    form_class = UserForm
    success_url = reverse_lazy('users:login')
    template_name = 'register.html'
