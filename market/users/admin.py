from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth import get_user_model

User = get_user_model()


@admin.register(User)
class UserAdmin(AuthUserAdmin):
    list_display = AuthUserAdmin.list_display + ('phone',)

    add_fieldsets = AuthUserAdmin.add_fieldsets + (
            (None, {
                'fields': ('phone', 'image'),
            }),
        )
    fieldsets = AuthUserAdmin.fieldsets + (
        (None, {
            'fields': ('phone', 'image'),
        }),
    )
