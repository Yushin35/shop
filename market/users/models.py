from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    image = models.ImageField(upload_to='users', blank=True)
    phone = models.CharField(blank=False,
                             null=False,
                             max_length=20)
    
    def __str__(self):
        return self.username
