from market.products.models import Order
from market.users.models import User


def purchase_mailing_body_generator(order_pk):
    order = Order.objects.get(pk=order_pk)
    order_items = order.items.all().select_related('product')

    body = 'Order #{0}\n'.format(order.pk)

    for order_item in order_items:
        body += 'Product name - {0}\nprice - {1}\namount - {2}\nsubtotal - {3}\n{4}'.format(
            order_item.product.name,
            order_item.price,
            order_item.amount,
            order_item.total,
            '-' * 50,
        )
    body += '\nTotal - {0}'.format(order.total)
    return body


def complete_purchase_body_generator():
    users = User.objects.filter(
        cart_items__isnull=False
    )

    subject = 'Complete purchase in shop'

    for user in users:
        body = 'Your cart:\n'
        cart_items = user.cart_items.all().select_related('product')
        for cart_item in cart_items:
            body += 'Product name - {0}\nPrice - {1}\nAmount - {2}\n{3}'.format(
                cart_item.product.name,
                cart_item.product.price,
                cart_item.amount,
                '-' * 50,
            )
        body += '\nPlease complete purchase!!!'
