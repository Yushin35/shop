from market.products.models import CartItem, OrderItem, Order


def get_ip_from_request(request):
    try:
        return request.META['HTTP_X_FORWARDED_FOR'].split(',')[-1].strip()
    except KeyError:
        return request.META['REMOTE_ADDR']


def get_carts_total(carts):
    return sum(
        cart.amount * cart.product.price
        for cart in carts
    )


def create_order_items(order, carts):
    order_items = []
    for cart in carts:
        order_items.append(OrderItem.objects.create(
            order=order,
            product=cart.product,
            amount=cart.amount,
            price=cart.product.price,
            total=cart.product.price * cart.amount
        ))
        cart.delete()
    return order_items


def create_paypal_payment_data(order_items, order):
    data = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"},
            "redirect_urls": {
                "return_url": "http://localhost:8000/payments/paypal/execute",
                "cancel_url": "http://localhost:8000/"},
            "transactions": [{
                "item_list": {
                    "items": [
                        {
                            "name": order_item.product.name,
                            "sku": "item",
                            "price": str(order_item.price),
                            "currency": "USD",
                            "quantity": order_item.amount
                        }
                        for order_item in order_items
                    ]
                },
                "amount": {
                    "total": str(order.total),
                    "currency": "USD"},
                "description": "This is the payment transaction description."}]
        }
    return data

