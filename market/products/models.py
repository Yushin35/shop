from django.contrib.postgres.fields import JSONField
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


from model_utils import Choices
from model_utils.models import TimeStampedModel

from ckeditor.fields import RichTextField
from config import settings


class Category(models.Model):
    """Product category model"""

    name = models.CharField(_('Name'), max_length=200)
    slug = models.SlugField(_('Slug'), unique=True)

    PARAMS = Choices(
        ('following', 'following'),
        ('price_to', 'price_to'),
        ('price_from', 'price_from'),
    )

    def __str__(self):
        return self.name
        

class SubCategory(models.Model):
    """Subcategory for category model"""

    name = models.CharField(max_length=200)
    slug = models.SlugField(_('Slug'), unique=True)

    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name='subcategories',
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            'products:subcategory_detail', 
            kwargs={
                'category_slug': self.category.slug,
                'subcategory_slug': self.slug
        })


class Product(TimeStampedModel):
    """Product model"""

    POPULAR_CHOICES = (
        ('F', 'not_popular'),
        ('T', 'popular')
    )

    name = models.CharField(_('Name'), max_length=200)
    code = models.CharField(
        null=False,
        blank=False,
        max_length=255,
    )
    in_stock = models.BooleanField(default=False)
    slug = models.SlugField(_('Slug'), unique=True)
    price = models.DecimalField(_('Price'), decimal_places=2, max_digits=9)
    description = RichTextField(_('Description'), blank=True)
    subcategory = models.ForeignKey(
        SubCategory,
        related_name='products',
        on_delete=models.CASCADE,
    )
    image = models.ImageField(upload_to='products', blank=True)
    popular = models.CharField(
        max_length=1,
        choices=POPULAR_CHOICES,
        blank=False,
        default='F',
    )
 
    class Meta:
        ordering = ('-created', )

    def get_absolute_url(self):
        return reverse('products:product_detail', kwargs={
            'category_slug': self.subcategory.category.slug,
            'subcategory_slug': self.subcategory.slug,
            'product_slug': self.slug
        })

    def __str__(self):
        return self.name


class ProductImage(models.Model):
    image = models.ImageField(upload_to='products', blank=True)
    product = models.ForeignKey(
        Product,
        related_name='images',
        on_delete=models.CASCADE, 
    )


class Like(TimeStampedModel):
    product = models.ForeignKey(
        Product,
        related_name='likes',
        on_delete=models.CASCADE,         
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='likes',
        on_delete=models.CASCADE,     
    )

    class Meta:
        unique_together = (
            ('product', 'user'),
        )

    def __str__(self):
        return '{} from {}'.format(self.product, self.user)


class Comment(TimeStampedModel):
    product = models.ForeignKey(
        Product,
        blank=False,
        null=False,
        related_name='comments',
        on_delete=models.CASCADE,     
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=False,
        null=False,
        related_name='comments',
        on_delete=models.CASCADE, 
    )

    text = models.TextField(_('Comment'))


class Checkout(models.Model):
    phone = models.CharField(
        max_length=255,
        null=False,
        blank=False,
    )
    payment_type = models.CharField(
        max_length=30,
        choices=[
            ('stripe', 'Stripe'),
            ('paypal', 'PayPal'),
            ('liqpay', 'Liqpay')
        ],
        null=False,
        blank=False,
    )
    fio = models.CharField(
        max_length=255,
        null=True,
        blank=True,
    )
    email = models.EmailField(null=True)
    comment = models.CharField(
        max_length=500,
        null=False,
        blank=False,
    )

    def __str__(self):
        return 'Checkout({}, {}, {}, {}, {})'.format(
            self.pk,
            self.phone,
            self.fio,
            self.email,
            self.comment,
        )


class Order(TimeStampedModel):
    PAY_STRIPE = 'stripe'
    PAY_PAYPAL = 'paypal'
    PAY_LIQPAY = 'liqpay'

    DEFAULT_PAYMENT_TYPE = 'liqpay'

    total = models.DecimalField(
        _('Total'),
        decimal_places=2,
        max_digits=9,
    )
    paid = models.BooleanField(default=False)
    approved = models.BooleanField(default=False)
    payment_type = models.CharField(
        choices=[
            ('stripe', 'Stripe'),
            ('paypal', 'PayPal'),
            ('liqpay', 'Liqpay')
        ],
        max_length=255,
    )
    payment_id = models.CharField(
        blank=True,
        max_length=255,
    )
    u_id = models.CharField(
        blank=False,
        null=False,
        max_length=255,
    )
    json_response = JSONField(null=True, blank=True)
    checkout = models.ForeignKey(
        Checkout,
        on_delete=models.CASCADE,
        null=False,
    )

    progress = models.CharField(
        max_length=2,
        choices=(
            ('f', 'wait'),
            ('t', 'ok'),
        ),
        default='f',
    )
    datetime = models.DateTimeField(null=False, default=timezone.now())

    def __str__(self):
        return 'Order_{}'.format(self.pk)


class CartItem(TimeStampedModel):
    product = models.ForeignKey(
        Product,
        related_name='cart',
        on_delete=models.CASCADE,
    )
    order = models.ForeignKey(
        Order,
        related_name='cart_items',
        on_delete=models.CASCADE,
    )
    quantity = models.PositiveIntegerField(_('Amount'))
    total = models.DecimalField(
        _('Total'),
        decimal_places=2,
        max_digits=9,
    )

    def __str__(self):
        return 'CartItem for {}'.format(self.order)


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order,
        related_name='items',
        on_delete=models.CASCADE,     
    )
    code = models.CharField(
        null=False,
        blank=False,
        max_length=255,
    )
    product = models.ForeignKey(
        Product,
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.SET_NULL,
    )
    name = models.CharField(blank=False, null=False, max_length=200)
    price = models.DecimalField(_('Price'), decimal_places=2, max_digits=9)
    description = RichTextField(_('Description'), blank=True)
    subcategory = models.CharField(blank=False, null=False, max_length=200)

    quantity = models.PositiveIntegerField(_('Quantity'))
    total = models.DecimalField(_('Total'), decimal_places=2, max_digits=9)
