from django.conf.urls import url
from market.products import views


urlpatterns = [
    url(r'^info/$', views.InfoView.as_view(), name='info'),
    url(r'^search/$', views.search_products, name='search_products'),
    url(r'^cart/$', views.CartView.as_view(), name='cart'),
    url(r'^clear_cart/$', views.ClearCart.as_view(), name='clear_cart'),
    url(
        r'^(?P<product_pk>[0-9]+)/add/to/cart/$',
        views.AddToCartView.as_view(),
        name='add_to_cart'
    ),
    url(
        r'^(?P<category_slug>.+)/(?P<subcategory_slug>.+)/(?P<product_slug>.+)/$',
        views.ProductDetailView.as_view(),
        name='product_detail'
    ),
    url(
        r'^(?P<category_slug>.+)/(?P<subcategory_slug>.+)/$',
        views.SubCategoryDetailView.as_view(),
        name='subcategory_detail'),
]
