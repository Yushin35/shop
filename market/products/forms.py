from django import forms
from django.contrib.auth import get_user_model

from market.products.models import Product, Comment


User = get_user_model()


class LikeForm(forms.Form):
    user = forms.ModelChoiceField(
        User.objects.all(),
        required=False
    )
    product = forms.ModelChoiceField(Product.objects.all())


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['user', 'product', 'text']


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'phone', 'email', 'password', 'image']
        widgets = {
            'username': forms.TextInput(attrs={
                'example': 'example',
                'class': 'wrap-input100 validate-input m-b-16 inputCustom',
                'placeholder': 'Укажите имя',
            }),
            'phone': forms.TextInput(attrs={
                'example': 'example',
                'class': 'wrap-input100 validate-input m-b-16 inputCustom',
                'placeholder': 'Укажите телефон',
            }),
            'email': forms.TextInput(attrs={
                'example': 'example',
                'class': 'wrap-input100 validate-input m-b-16 inputCustom',
                'placeholder': 'Укажите адрес эл.почты',
            }),
            'password': forms.TextInput(attrs={
                'type':'password',
                'example': 'example',
                'class': 'wrap-input100 validate-input m-b-16 inputCustom password_input',
                'placeholder': 'Пароль',
            }),
           
        }

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user
