import json

from collections import namedtuple
from datetime import timedelta, datetime

from django.template import loader
from django.db.models import (
    Count,
)

from django.utils.decorators import method_decorator
from django.http import Http404
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.db.utils import IntegrityError
from django.core import paginator

from django.core import serializers
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from watson import search as search_watson

from braces.views import JSONResponseMixin, AjaxResponseMixin

from market.products.forms import LikeForm, CommentForm
from market.products.models import (
    Category,
    SubCategory,
    Product,
)
from django.views import View
from market.products.cart import Cart

WeekDaysInterval = namedtuple('WeekDaysInterval', ["start_date", "end_date"])


class CategoryListView(ListView):
    model = Category
    context_object_name = 'category_list'
    template_name = 'products/subcategory_list.html'

    @staticmethod
    def get_most_popular_products_of_all(products_count=9):
        most_popular_products = (Product.objects.all().filter(
            popular='T',
        ).select_related('subcategory')[:products_count])
        return most_popular_products

    @staticmethod
    def get_new_arrivals(days_ago=5):
        curr_time = datetime.now().date()
        time_from = curr_time - timedelta(days=days_ago)
        new_arrivals = (Product.objects.filter(created__gt=time_from).select_related('subcategory').
                        annotate(product_likes=Count('likes')))
        
        return new_arrivals[:4]

    def get_queryset(self):
        return Category.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['most_popular_products'] = CategoryListView.get_most_popular_products_of_all()
        context['new_arrivals'] = self.get_new_arrivals()

        return context


class SubCategoryDetailView(DetailView):
    model = SubCategory
    template_name = 'products/subcategory_detail.html'
    slug_url_kwarg = 'subcategory_slug'
    PARAM_FOLLOWING = 'following'
    PARAM_PRICE_FROM = 'price_from'
    PARAM_PRICE_TO = 'price_to'
    products_paginate_by = 7
    active_tab = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['subcategory_list'] = SubCategory.objects.select_related('category').all()
        context['category_list'] = Category.objects.all()
        products = Product.objects.filter(
            subcategory=kwargs['object'],
        ).select_related('subcategory').annotate(
            product_likes=Count('likes'),
        ).annotate(
            product_comments=Count('comments'),
        )

        products_page = self.request.GET.get('page', None)
        products_paginator = paginator.Paginator(products, self.products_paginate_by)
        try:
            products_page_obj = products_paginator.page(products_page)
        except paginator.InvalidPage:
            products_page_obj = products_paginator.page(1)
        context['products'] = products_page_obj
        context['is_paginated'] = True

        context['active_tab'] = self.object.slug

        return context


class ProductDetailView(AjaxResponseMixin, JSONResponseMixin, DetailView):
    model = Product
    slug_url_kwarg = 'product_slug'
    time_delta_display_comments = timedelta(hours=24)
    comments = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        end_datetime = datetime.now()
        start_datetime = end_datetime - self.time_delta_display_comments
        context['comments_'] = self.object.comments.filter(
            created__gt=start_datetime,
            created__lt=end_datetime).select_related('user').order_by('created')
        context['most_popular_products'] = CategoryListView.get_most_popular_products_of_all()
        context['category_list'] = Category.objects.all()
        return context

    def get_queryset(self):
        return Product.objects.select_related('subcategory').annotate(product_likes=Count('likes'))

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        context_dict = self.add_comment(request)
        return self.render_json_response(context_dict)

    @staticmethod
    def add_comment(request):
        context_dict = dict()

        raw_data = {
            'user': request.user.id,
            'text': request.POST.get('text'),
            'product': request.POST.get('product'),
        }
        form = CommentForm(raw_data)
        if form.is_valid():
            context_dict['data'] = loader.get_template(
                'additional_parts/products/product_detail_comment.html'
            ).render(dict(comment=form.save()))

            return context_dict

        context_dict['errors'] = form.errors

        return context_dict


@method_decorator(csrf_exempt, name='dispatch')
class AddToCartView(JSONResponseMixin, View):

    def __init__(self, **kwargs):
        self.product = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        try:
            self.product = Product.objects.get(id=kwargs['product_pk'])
        except Product.DoesNotExist:
            raise Http404()

        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        print('asd')
        try:
            amount = int(request.POST.get('product_amount', 1))

            print('asd', amount)

            if amount < 1:
                raise ValueError
        except ValueError as err:
            print(request.POST.get('product_amount', 1))
            return self.render_json_response({'status': 'error'})

        cart = Cart(request)
        cart.add(self.product, amount, True)

        return self.render_json_response(
            {
                'status': 'added',
                'product_id': self.product.id,
                'item_total_price': cart.get_item_total_price(self.product),
                'total': cart.get_total_price()
            }
        )

    def delete(self, request, *args, **kwargs):
        cart = Cart(request)
        cart.remove(self.product)

        return self.render_json_response(
            {
                'status': 'deleted',
                'total': cart.get_total_price()
            }
        )


@method_decorator(csrf_exempt, name='dispatch')
class ClearCart(JSONResponseMixin, View):
    def delete(self, request, *args, **kwargs):
        Cart(request).clear()

        return self.render_json_response({'status': 'cleared'})


class CartView(TemplateView):
    active_tab = 'cart'
    template_name = 'products/cart.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        cart = Cart(self.request)
        context['cart_list'] = cart.get_products()
        context['total'] = cart.get_total_price()

        return context

# remove when add jquery
@csrf_exempt
@require_http_methods(['POST'])
def search_products(request):
    search_result = search_watson.search(
        request.POST.get('value', ''),
    )
    
    return JsonResponse(
        data=json.loads(
            serializers.serialize('json', search_result)
        ),
        safe=False,
    )


class InfoView(TemplateView):
    template_name = 'info.html'
