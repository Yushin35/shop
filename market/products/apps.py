from django.apps import AppConfig
from watson import search as watson


class ProductsConfig(AppConfig):
    name = 'market.products'
    
    def ready(self):
        Product = self.get_model("Product")
        watson.register(Product)
