from django.contrib import admin

from market.products.models import (
    Category,
    SubCategory,
    Product,
    Like,
    Comment,
    CartItem,
    ProductImage
)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = (
        'pk',
        'name',
        'slug',
    )


@admin.register(SubCategory)
class SubCategoryAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = (
        'pk',
        'name',
        'slug',
        'category',
    )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = (
        'pk',
        'name',
        'in_stock',
        'slug',
        'price',
        'description',
        'popular',
        'subcategory',
        'image',
    )


@admin.register(ProductImage)
class ProductImageAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = (
        'pk',
        'image',
        'product',
    )


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = (
        'pk',
        'user',
        'product',
    )


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = (
        'pk',
        'product',
        'user',
        'text',
        'created',
    )


@admin.register(CartItem)
class CartItemAdmin(admin.ModelAdmin):
    view_on_site = False
