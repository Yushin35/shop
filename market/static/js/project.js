function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;

}

function init_token() {
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(POST|GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

function add_to_cart_response(response) {
    console.log(response);
    if(response.status=="deleted"){
        if($(".artikul_products").length>1){
        showLoader();}
        hideLoader();
        setTimeout(function(){
        $(".artikul_products").filter(("[data-productIdRemove ="+product+"]")).remove();  },1000);
        $("#total_cart_price").text("Всего к оплате :  " + response['total'] + " грн");
        if($(".artikul_products").length<=1){
            cartNull();
           
          }
     } 
}


function clear_cart_response(data) {
    console.log(data);
    if(data.status=="cleared"){showLoader();
        setTimeout(function(){  cartNull()},1500)};
        hideLoader();
}


function send_ajax_request(request_url, data, success_func, method='POST', data_type='json') {
    $.ajax({
        url: request_url,
        type: method,
        dataType: data_type,
        data: data,
        success: function(ajax_response) {
            success_func(ajax_response);
        },
        error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

$(document).ready(function() {
    init_token();
});
