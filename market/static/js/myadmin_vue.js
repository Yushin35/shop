var app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        orders: [],
        order_items: [],
        next_page: null,
        prev_page: null,
        curr_page_num: 1,
        progress: 'f',
        paid: 't',
        checkout: null,
        loading: false,
        currentArticle: {},
        message: "Hello Vue!",
    },
    mounted: function() {
        this.updateOrdersCurrPage();
    },
    methods: {
        fetchOrderDataFromResponse: function(response) {
            console.log(response);
            this.orders = response.data.results;
            this.next_page = response.data.next;
            this.curr_page_num = response.data.current;
            this.prev_page = response.data.previous;
            this.loading = false;
        },
        updateOrdersCurrPage: function() {
            this.loading = true;
            var query_string = "&progress=" + this.progress + "&page=" + this.curr_page_num;
            axios.get("/myadmin/orders/?paid=" + this.paid + query_string)
                 .then((response) => {
                    this.fetchOrderDataFromResponse(response);
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        getOrders: function() {
            this.loading = true;
             axios.get("/myadmin/orders/")
                 .then((response) => {
                    this.fetchOrderDataFromResponse(response);
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        getNextPrevOrdersPage: function(url) {
            this.loading = true;
             axios.get(url)
                 .then((response) => {
                    this.fetchOrderDataFromResponse(response);
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        getOrderItems: function(id, checkout) {
             this.loading = true;
             this.checkout = checkout;

             axios.get("/myadmin/order_items/?order_pk=" + id)
                 .then((response) => {
                    this.order_items = response.data.results;
                    //this.orders = response.data.results;
                    this.loading = false;
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        onChangeOrderProgress: function(event) {
            this.progress = event.target.value;
            var query_string = "";

            this.loading = true;
            if(this.progress == 'all') {
                query_string = "";
            } else {
                query_string = "&progress=" + this.progress;
            }
            axios.get("/myadmin/orders/?paid=" + this.paid + query_string)
                 .then((response) => {
                    this.fetchOrderDataFromResponse(response);
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        onChangeOrderPaidStatus: function(event) {
            this.paid = event.target.value;
            var query_string = "";

            this.loading = true;
            if(this.progress == 'all') {
                query_string = "";
            } else {
                query_string = "&progress=" + this.progress;
            }
            axios.get("/myadmin/orders/?paid=" + this.paid + query_string)
                 .then((response) => {
                    this.fetchOrderDataFromResponse(response);
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        changeProgress: function(id, progress) {
            this.loading = true;
            axios.patch("/myadmin/orders/" + id + "/", {
                     "id": id,
                     "progress": "t"
             })
                 .then((response) => {
                    console.log(response.data);
                    this.updateOrdersCurrPage();
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        approveOrder: function(id) {
            this.loading = true;
            axios.patch("/myadmin/orders/" + id + "/", {
                     "id": id,
                     "approved": true
             })
                 .then((response) => {
                    console.log(response.data);
                    this.updateOrdersCurrPage();
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        },
        dropOrder: function(id) {
            this.loading = true;
            axios.delete("/myadmin/orders/" + id + "/")
                 .then((response) => {
                    console.log(response.data);
                    this.updateOrdersCurrPage();
            })
            .catch((err) => {
            this.loading = false;
                console.log(err);
            })
        }
    }
});
