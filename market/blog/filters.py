from rest_framework import filters


class StatusFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        progress = request.query_params.get('progress')
        if progress:
            return queryset.filter(progress=progress)
        return queryset


class PaidStatusFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        paid = request.query_params.get('paid')
        if paid:
            return queryset.filter(paid=paid)
        return queryset


class OrderItemOrderPkFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        order_pk = request.query_params.get('order_pk')
        if order_pk:
            return queryset.filter(order__pk=order_pk)
        return queryset
