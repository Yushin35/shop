from django.db import models
from django.conf import settings

from pygments import highlight
from pygments.lexers import (
    PythonLexer,
    CLexer,
    CppLexer,
    BashLexer,
)

from pygments.formatters import HtmlFormatter


UserModel = settings.AUTH_USER_MODEL


class Post(models.Model):
    title = models.fields.CharField(max_length=255, null=False,
                                    blank=False)
    author = models.ForeignKey(UserModel, on_delete=models.CASCADE,
                               blank=False, null=False,)
    text = models.fields.CharField(max_length=2000, null=False,
                                   blank=False)


class Snippet(models.Model):
    CODE_LANG = (
        ('c', 'c'),
        ('cpp', 'cpp'),
        ('python', 'python'),
        ('bash', 'bash'),
    )
    LEXERS = {
        'c': CLexer,
        'cpp': CppLexer,
        'python': PythonLexer,
        'bash': BashLexer,
    }

    lang = models.CharField(choices=CODE_LANG, default='python',
                            max_length=50, blank=False, null=False)
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE,
        null=False, blank=False, related_name='snippets')
    code = models.CharField(max_length=2000, null=False,
                            blank=False)

    def __str__(self):
        formatted_code = highlight(self.code,
                                   self.LEXERS[str(self.lang)](),
                                   HtmlFormatter())
        return '%s\n%s' % (self.lang, formatted_code)
