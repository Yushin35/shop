#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z db 5432; do
  sleep 1
done


/usr/bin/python3 manage.py makemigrations
/usr/bin/python3 manage.py migrate
/usr/bin/python3 manage.py collectstatic --no-input

exec "$@"
