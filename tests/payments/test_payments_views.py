from unittest.mock import patch
from pytest import fixture, raises

import re
from itertools import zip_longest

from market.payments.views import (
    CheckoutView,
    PayStripeView,
    OrderListView,
)
from market.products.models import (
    CartItem,
    OrderItem,
    Order,
    Product,
    Category,
)

from market.users.models import User
from market.payments.forms import CheckoutForm


USER_PASSWORD = '123'


@fixture
def cart_items(db, user, products):
    return [
        CartItem.objects.create(
            user=user,
            product=product,
            amount=1,
        )
        for product in products
    ]


@fixture
def order_items(db, cart_items, order):
    order_items = []
    for cart in cart_items:
        order_items.append(OrderItem.objects.create(
            order=order,
            product=cart.product,
            amount=cart.amount,
            price=cart.product.price,
            total=cart.product.price * cart.amount
        ))
        cart.delete()
    return order_items


@fixture
def order(db, user):
    return Order.objects.create(
        user=user,
        total=300,
    )


@fixture
def user(db, django_user_model):
    username = "test_user"
    password = USER_PASSWORD
    return django_user_model.objects.create_user(username=username, password=password)


@fixture
def category(db):
    return Category.objects.create(name='test_name_1', slug='test_1')


@fixture
def products(db, category):
    return [
        Product.objects.create(
            name='product_name _%d' % (i, ),
            slug='product_slug__%d' % (i, ),
            price=100,
            description='some description',
            category=category,
            popular='T',
            grade='premium',
        )
        for i in range(3)
    ]


def test_checkout_view_post(client, user, cart_items, products):
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.post('/payments/checkout/', {
        'payment_type': 'stripe'
    })
    pk = int(re.search(r'(\d+)', response.url).group())
    order = Order.objects.get(pk=pk)

    def sort_key(item):
        return item.product_id, item.amount

    order_items = sorted(order.items.all(), key=sort_key)
    cart_items = sorted(cart_items, key=sort_key)

    for order_item, cart_item in zip_longest(order_items, cart_items):
        assert order_item and cart_item
        assert order_item.product_id == cart_item.product_id
        assert order_item.amount == cart_item.amount
        assert order_item.total == cart_item.amount * cart_item.product.price


def test_pay_stripe_view_get_context(client, user, order, order_items, products, settings):
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.get('/payments/paystripe/{}/'.format(order.pk))
    order_items_ids = sorted(order_item.pk for order_item in response.context['order_items'])

    assert response.context['order'].pk == order.pk
    assert response.context['order'] == order
    assert response.context['stripe_public_key'] == settings.STRIPE_PUBLIC_KEY
    assert order_items_ids == sorted(order_item.pk for order_item in order_items)


def test_pay_paypal_view_get_context(client, user, order, order_items, products, settings):
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.get('/payments/paypal/{}/'.format(order.pk))
    order_items_ids = sorted(order_item.pk for order_item in response.context['order_items'])

    assert response.context['order'].pk == order.pk
    assert response.context['client_id'] == settings.PAYPAL_CLIENT_ID
    assert order_items_ids == sorted(order_item.pk for order_item in order_items)


def test_checkout_view_get_context(client, user):
    checkout_form = CheckoutForm()
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.get('/payments/checkout/')

    assert response.context['checkout_form'].as_p() == checkout_form.as_p()


def test_checkout_view_post_without_cart_items(client, user):
    checkout_form = CheckoutForm()
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.post('/payments/checkout/')

    assert response.url == '/'


def test_checkout_view_post_with_cart_items(client, user, cart_items):
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.post('/payments/checkout/', {
        'payment_type': 'stripe'
    })
    pk = int(re.search(r'(\d+)', response.url).group())
    order = Order.objects.get(pk=pk)

    def sort_key(item):
        return item.product_id, item.amount

    order_items = sorted(order.items.all(), key=sort_key)
    cart_items = sorted(cart_items, key=sort_key)

    for order_item, cart_item in zip_longest(order_items, cart_items):
        assert order_item and cart_item
        assert order_item.product_id == cart_item.product_id
        assert order_item.amount == cart_item.amount
        assert order_item.total == cart_item.amount * cart_item.product.price


def test_checkout_view_post_wrong_type(client, user, cart_items):
    client.login(username=user.username, password=USER_PASSWORD)
    with raises(KeyError):
        response = client.post('/payments/checkout/', {
            'payment_type': 'wrong_payment_type'
        })


@patch('market.payments.views.paypalrestsdk')
def test_paypal_view_post_charge_created(mock_paypalrestsdk, client, user, order, order_items, products):
    paymentID = 'xrf199'
    client.login(username=user.username, password=USER_PASSWORD)
    payment = mock_paypalrestsdk.Payment.return_value
    payment.create.return_value = True
    payment.id = paymentID

    response = client.post('/payments/paypal/{0}/'.format(order.pk))
    order.refresh_from_db()

    payment.create.assert_called_once()

    assert response.json()['paymentID'] == paymentID
    assert order.payment_id == paymentID
    assert order.payment_type == 'paypal'


@patch('market.payments.views.paypalrestsdk')
def test_paypal_view_post_charge_not_created(mock_paypalrestsdk, client, user, order, order_items, products):
    client.login(username=user.username, password=USER_PASSWORD)

    payment = mock_paypalrestsdk.Payment.return_value
    payment.create.return_value = False

    response = client.post('/payments/paypal/{0}/'.format(order.pk))
    order.refresh_from_db()

    payment.create.assert_called_once()

    assert response.json()['paymentID'] is None
    assert order.payment_id == ''
    assert order.payment_type == ''


@fixture
def paypal_order(db, user):
    return Order.objects.create(
        user=user,
        total=300,
        payment_id='123',
        payment_type='paypal',
    )


@patch('market.payments.views.paypalrestsdk')
def test_paypal_execute_view_successfull(mock_paypalrestsdk, client, user, paypal_order):
    client.login(username=user.username, password=USER_PASSWORD)

    payment = mock_paypalrestsdk.Payment.find.return_value
    payment.execute.return_value = True

    response = client.post('/payments/paypal/execute/', {
        'paymentID': paypal_order.payment_id,
        'payerID': 1
    })

    paypal_order.refresh_from_db()

    assert response.json()['payment_status'] == 'ok'
    assert paypal_order.paid


@patch('market.payments.views.paypalrestsdk')
def test_paypal_execute_view_unsuccessfull(mock_paypalrestsdk, client, user, paypal_order):
    client.login(username=user.username, password=USER_PASSWORD)

    payment = mock_paypalrestsdk.Payment.find.return_value
    payment.execute.return_value = False

    response = client.post('/payments/paypal/execute/', {
        'paymentID': paypal_order.payment_id,
        'payerID': 1
    })

    paypal_order.refresh_from_db()

    assert response.json()['payment_status'] == 'fail'
    assert not paypal_order.paid


@patch('market.payments.views.stripe')
def test_pay_stripe_view_post(mock_stripe, client, user, order, order_items, products):
    mock_charge = mock_stripe.Charge.create.return_value
    mock_charge.__getitem__.return_value = 'abc123'

    client.login(username=user.username, password=USER_PASSWORD)
    response = client.post('/payments/paystripe/{}/'.format(order.pk), {'stripeToken': 'test'})

    mock_charge.__getitem__.assert_called_once_with('id')
    mock_charge.save.assert_called_once()

    order.refresh_from_db()

    assert order.paid
    assert order.payment_id == 'abc123'


def test_order_list_view(client, user, order, order_items, products):
    client.login(username=user.username, password=USER_PASSWORD)
    response = client.get('/payments/orders/')
    assert list(response.context['order_list']) == [order, ]
