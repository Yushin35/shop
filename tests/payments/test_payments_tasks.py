from unittest.mock import patch
from pytest import fixture, raises

from django.core import mail


from market.payments.tasks import purchase_mailing, complete_purchase
from market.users.models import User
from market.products.models import (
    Order,
    OrderItem,
    CartItem,
    Category,
    Product,
)


@fixture
def user(db):
    user = User.objects.create_user(
        username='test',
        email='test@mail.com',
    )
    user.set_password('123')
    user.save()
    return user


@fixture
def category(db):
    return Category.objects.create(name='test_name_1', slug='test_1')


@fixture
def product(db, category):
    return Product.objects.create(
            name='product_name',
            slug='product_slug',
            price=100,
            description='some description',
            category=category,
            popular='T',
            grade='premium',
        )


@fixture
def order(db, user):
    return Order.objects.create(
        user=user,
        total=300,
    )


@fixture
def order_item(db, user, product, order):
    return OrderItem.objects.create(
            order=order,
            product=product,
            amount=1,
            price=product.price,
            total=product.price,
        )


@fixture
def cart_item(db, user, product):
    return CartItem.objects.create(
        product=product,
        user=user,
        amount=1,
    )


@patch('market.payments.tasks.purchase_mailing_body_generator')
def test_purchase_mailing(mock_purchase_mailing_body_generator, user, order, order_item):
    mock_purchase_mailing_body_generator.return_value = 'body'
    purchase_mailing(user.email, order.pk)

    assert len(mail.outbox) == 1
    assert mail.outbox[0].body == 'body'


def test_complete_purchase(cart_item):
    complete_purchase()
    assert len(mail.outbox) == 1
