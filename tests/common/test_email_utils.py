from pytest import fixture

from market.common.email_utils import purchase_mailing_body_generator
from market.users.models import User
from market.products.models import (
    Order,
    OrderItem,
    CartItem,
    Category,
    Product,
)


@fixture
def user(db):
    user = User.objects.create_user(
        username='test',
        email='test@mail.com',
    )
    user.set_password('123')
    user.save()
    return user


@fixture
def category(db):
    return Category.objects.create(name='test_name_1', slug='test_1')


@fixture
def product(db, category):
    return Product.objects.create(
            name='product_name',
            slug='product_slug',
            price=100.00,
            description='some description',
            category=category,
            popular='T',
            grade='premium',
        )


@fixture
def order(db, user):
    return Order.objects.create(
        user=user,
        total=100.00,
    )


@fixture
def order_item(db, user, product, order):
    return OrderItem.objects.create(
            order=order,
            product=product,
            amount=1,
            price=product.price,
            total=product.price,
        )


def test_purchase_mailing_body_generator(user, order_item):
    body = purchase_mailing_body_generator(order_item.order.pk)
    assert body == 'Order #{0}\nProduct name - product_name\nprice - 100.00\namount - 1\nsubtotal - 100.00\n{1}\nTotal - 100.00'.format(order_item.order.pk, '-'*50)


def test_complete_purchase_body_generator(user):
    cart_items = user.cart_items.all().select_related('product')
    for cart_item in cart_items:
        body = ''
        body += '''
                    Product name - {0}
                    Price - {1}
                    Amount - {2}
                    {3}
                '''.format(
            cart_item.product.name,
            cart_item.product.price,
            cart_item.amount,
            '-' * 50,
        )
    body += '\nPlease complete purchase!!!'
    return body
