from itertools import zip_longest
from unittest.mock import MagicMock
from pytest import fixture

from market.common.utils import get_ip_from_request
from market.common.utils import get_carts_total, create_order_items
from market.products.models import (
    CartItem,
    OrderItem,
    Order,
    Product,
    Category,
)
from market.users.models import User


USER_PASSWORD = '123'


@fixture
def cart_items(db, user, products):
    return [
        CartItem.objects.create(
            user=user,
            product=product,
            amount=1,
        )
        for product in products
    ]


@fixture
def order_items(db, cart_items, order):
    order_items = []
    for cart in cart_items:
        order_items.append(OrderItem.objects.create(
            order=order,
            product=cart.product,
            amount=cart.amount,
            price=cart.product.price,
            total=cart.product.price * cart.amount
        ))
        cart.delete()
    return order_items

@fixture
def order(db, user):
    return Order.objects.create(
        user=user,
        payment_type='stripe',
        total=300,
    )


@fixture
def user(db, django_user_model):
    username = "test_user"
    password = USER_PASSWORD
    return django_user_model.objects.create_user(username=username, password=password)


@fixture
def category(db):
    return Category.objects.create(name='test_name_1', slug='test_1')


@fixture
def products(db, category):
    return [
        Product.objects.create(
            name='product_name _%d' % (i, ),
            slug='product_slug__%d' % (i, ),
            price=100,
            description='some description',
            category=category,
            popular='T',
            grade='premium',
        )
        for i in range(3)
    ]


class TestGetIPFromRequest:
    def test_x_forwarded(self):
        request = MagicMock()
        request.META = {'HTTP_X_FORWARDED_FOR': '127.0.0.1,192.168.1.1'}
        assert get_ip_from_request(request) == '192.168.1.1'


def test_get_carts_total(cart_items, products):
    assert get_carts_total(cart_items) == 300
