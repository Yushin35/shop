from pytest import fixture

from market.products.models import Product, Category, Like
from market.products.views import (
    CategoryListView,
)

from django.contrib.auth import get_user_model

User = get_user_model()


@fixture
def categories(db):
    return [
        Category.objects.create(name='test_name_1', slug='test_1'),
    ]


@fixture
def standard_products(db, categories):
    return [
        Product.objects.create(
            name='product_name _%d' % (i, ),
            slug='product_slug__%d' % (i, ),
            price=i*10,
            description='some description',
            category=categories[0],
            popular='T',
        )
        for i in range(10)
    ]


class TestCategoryListView:
    def test_get_most_popular_products_of_all(self, categories):
        popular_items = CategoryListView.get_most_popular_products_of_all(2)
