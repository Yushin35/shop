# Install project

* install redis
* install postgres

* cp env.example .env
* configure postgres
* configure .env file

* install python3 from https://www.python.org/
*  sudo apt-get install python3-pip 
* python -m venv env
* . env/bin/activate
* pip install -r requirements.txt
* python manage.py makemigrations
* python manage.py migrate
* python manage.py runserver
