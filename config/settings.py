import os
import sys
from importlib.util import find_spec

import stripe
import paypalrestsdk

from getenv import env
import dj_database_url


ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
APPS_DIR = os.path.join(ROOT_DIR, 'market')

DOCKER = env('DOCKER', default=False)

if not DOCKER:
    from dotenv import read_dotenv
    read_dotenv(os.path.join(ROOT_DIR, '.env'))

DEBUG = env('DEBUG', default=False)

SECRET_KEY = env('SECRET_KEY', required=True)
DATABASE_URL = env('DATABASE_URL', required=False)

ALLOWED_HOSTS = env('ALLOWED_HOSTS', default=['*'] if DEBUG else [])

USE_DEBUG_TOOLBAR = env('USE_DEBUG_TOOLBAR', default=False)

STRIPE_PUBLIC_KEY = env('STRIPE_PUBLIC_KEY')
STRIPE_SECRET_KEY = env('STRIPE_SECRET_KEY')

PAYPAL_MODE = env('PAYPAL_MODE')
PAYPAL_ACCESS_TOKEN = env('PAYPAL_ACCESS_TOKEN')
PAYPAL_CLIENT_ID = env('PAYPAL_CLIENT_ID')
PAYPAL_CLIENT_SECRET = env('PAYPAL_CLIENT_SECRET')

stripe.api_key = STRIPE_SECRET_KEY
paypal_api = paypalrestsdk.Api({
    'mode': PAYPAL_MODE,
    'client_id': PAYPAL_CLIENT_ID,
    'client_secret': PAYPAL_CLIENT_SECRET
})

LIQPAY_PUBLIC_KEY = env('LIQPAY_PUBLIC_KEY')
LIQPAY_PRIVATE_KEY = env('LIQPAY_PRIVATE_KEY')

CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = env('CELERY_RESULT_BACKEND')
CELERY_IMPORTS = ('market.payments.tasks',)


ADMIN_EMAIL = 'admin@mail.com'

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'


TIME_ZONE = 'UTC'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = False

CKEDITOR_UPLOAD_PATH = 'uploads/'

if DOCKER:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'postgres',
            'USER': 'postgres',
            'HOST': 'db',  # set in docker-compose.yml
            'PORT': 5432  # default postgres port
        }
    }
else:
    DATABASES = {
        'default': dj_database_url.parse(DATABASE_URL, conn_max_age=600),
    }

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
]

THIRD_PARTY_APPS = [
    'sorl.thumbnail',
    'ckeditor',
    'ckeditor_uploader',
    'django_extensions',
    'watson',
    'rest_framework',
    'rest_framework_swagger',
]

LOCAL_APPS = [
    'market.users.apps.UsersConfig',
    'market.products.apps.ProductsConfig',
    'market.payments.apps.PaymentsConfig',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

AUTH_USER_MODEL = 'users.User'
ADMIN_URL = r'^admin/'
LOGIN_URL = 'users:login'
LOGIN_REDIRECT_URL = 'home'
LOGOUT_REDIRECT_URL = 'home'

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(APPS_DIR, 'templates')
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

STATICFILES_DIRS = [
    os.path.join(APPS_DIR, 'static'),
]

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(ROOT_DIR, '.static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(ROOT_DIR, '.media')

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)


CART_SESSION_ID = 'cart'

LIQPAY_CALLBACK_URL = 'https://www.iot-seller.com/payments/pay-callback/'
