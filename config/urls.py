# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from market.products.views import CategoryListView


urlpatterns = [
    url(r'^$', CategoryListView.as_view(), name="home"),
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),
    url(r'^products/', include(("market.products.urls", "products"), namespace="products")),
    url(r'^users/', include(("market.users.urls", "users"), namespace="users")),
    url(r'^payments/', include(("market.payments.urls", "payments"), namespace="payments")),
    url(r'^myadmin/', include(("market.myadmin.urls", "myadmin"), namespace="myadmin")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
